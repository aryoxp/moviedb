package ap.mobile.made.moviedb;

import android.graphics.Bitmap;

public class Movie {

    public String title;
    public String originalTitle;
    public String overview;
    public String releaseDate;
    public String poster;

    public Movie(String title, String originalTitle, String overview,
                 String releaseDate, String poster) {
        this.title = title;
        this.originalTitle = originalTitle;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.poster = poster;
    }

    private Bitmap posterBitmap;

    public void setPosterBitmap(Bitmap bitmap) {
        this.posterBitmap = bitmap;
    }

    public Bitmap getPosterBitmap() {
        return posterBitmap;
    }
}
