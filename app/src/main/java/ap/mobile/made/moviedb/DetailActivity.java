package ap.mobile.made.moviedb;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivPoster;
    private boolean isImageFitToScreen;
    private ViewGroup.LayoutParams originalLayoutParams;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_detail);

        Intent intent = this.getIntent();

        String title = intent.getStringExtra("title");
        String originalTitle = intent.getStringExtra("original_title");
        String overview = intent.getStringExtra("overview");
        String posterPath = intent.getStringExtra("poster");
        String releaseDate = intent.getStringExtra("release");

        this.ivPoster = this.findViewById(R.id.iv_poster_detail);
        ((TextView) this.findViewById(R.id.tv_title_detail)).setText(title);
        ((TextView) this.findViewById(R.id.tv_original_title_detail)).setText(originalTitle);
        ((TextView) this.findViewById(R.id.tv_overview_detail)).setText(overview);
        ((TextView) this.findViewById(R.id.tv_release_date_detail)).setText(releaseDate);
        ((TextView) this.findViewById(R.id.tv_loading_poster)).setText(R.string.loading_poster);

        try {
            Picasso.with(this).load(CDM.PosterLargeBasePath + posterPath).into(this.ivPoster);
        } catch (Exception ex) {
            Log.e(CDM.AppTag, ex.getMessage());
        }

        this.ivPoster.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_poster_detail: // toggle poster fullscreen
                if (isImageFitToScreen) {
                    this.findViewById(R.id.fl_poster_container).setLayoutParams(originalLayoutParams);
                    this.findViewById(R.id.tv_large_toggle).setVisibility(View.VISIBLE);
                    this.ivPoster.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    this.isImageFitToScreen = false;
                } else {
                    this.originalLayoutParams = this.findViewById(R.id.fl_poster_container).getLayoutParams();
                    this.findViewById(R.id.fl_poster_container)
                            .setLayoutParams(
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                    );
                    this.findViewById(R.id.tv_large_toggle).setVisibility(View.GONE);
                    this.ivPoster.setScaleType(ImageView.ScaleType.FIT_XY);
                    this.isImageFitToScreen = true;
                }
                break;
        }
    }
}
