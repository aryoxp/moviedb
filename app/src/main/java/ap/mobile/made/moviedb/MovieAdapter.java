package ap.mobile.made.moviedb;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private IMovieAdapterListener movieAdapterListener;
    private ArrayList<Movie> movies;

    public MovieAdapter(ArrayList<Movie> movies, IMovieAdapterListener movieAdapterListener) {
        this.movies = movies;
        this.movieAdapterListener = movieAdapterListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Movie movie = this.movies.get(position);
        if(movie.getPosterBitmap() != null)
            holder.ivPoster.setImageBitmap(movie.getPosterBitmap());
        else {
            new BitmapLoader(holder.ivPoster).execute(movie);
        }
        holder.tvTitle.setText(movie.title);
        holder.tvOriginalTitle.setText(movie.originalTitle);
        holder.tvOverview.setText(movie.overview);
        holder.tvReleaseDate.setText(movie.releaseDate);

        holder.bind(movie, this.movieAdapterListener);
    }

    @Override
    public int getItemCount() {
        return this.movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivPoster;
        public TextView tvTitle;
        public TextView tvOriginalTitle;
        public TextView tvOverview;
        public TextView tvReleaseDate;

        public ViewHolder(View itemView) {
            super(itemView);
            this.ivPoster = itemView.findViewById(R.id.iv_poster);
            this.tvTitle = itemView.findViewById(R.id.tv_title);
            this.tvOriginalTitle = itemView.findViewById(R.id.tv_original_title);
            this.tvOverview = itemView.findViewById(R.id.tv_overview);
            this.tvReleaseDate = itemView.findViewById(R.id.tv_release_date);
        }

        public void bind(final Movie movie, final IMovieAdapterListener movieAdapterListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(movieAdapterListener != null) {
                        movieAdapterListener.onItemClicked(movie);
                    }
                }
            });
        }
    }

    public class BitmapLoader extends AsyncTask<Movie, Void, Bitmap> {

        private ImageView imageContainer;

        public BitmapLoader(ImageView imageContainer) {
            this.imageContainer = imageContainer;
        }

        @Override
        protected Bitmap doInBackground(Movie... movies) {
            try {

                URL url = new URL(CDM.PosterThumbnailBasePath + movies[0].poster);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap posterBitmap = BitmapFactory.decodeStream(input);
                connection.disconnect();

                movies[0].setPosterBitmap(posterBitmap);

                return posterBitmap;
            } catch (IOException e) {
                Log.e(CDM.AppTag, "Unable to load movie poster from URL");
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            this.imageContainer.setImageBitmap(bitmap);
        }
    }

    public interface IMovieAdapterListener {
        void onItemClicked(Movie movie);
    }

}
