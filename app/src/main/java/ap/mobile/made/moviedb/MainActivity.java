package ap.mobile.made.moviedb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SearchTask.ISearch, MovieAdapter.IMovieAdapterListener {

    private RecyclerView rvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.findViewById(R.id.bt_search).setOnClickListener(this);
        this.findViewById(R.id.v_searching).setVisibility(View.GONE);
        this.rvResult = this.findViewById(R.id.rv_result);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_search:
                String keyword = ((EditText) this.findViewById(R.id.et_keyword)).getText().toString();
                ((TextView) this.findViewById(R.id.tv_search_keyword)).setText("Searching: " + keyword);
                new SearchTask(this).execute(keyword);
                this.findViewById(R.id.v_searching).setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onSearchResult(ArrayList<Movie> movies) {
        this.findViewById(R.id.v_searching).setVisibility(View.GONE);
        MovieAdapter adapter = new MovieAdapter(movies, this);
        this.rvResult.setLayoutManager(new LinearLayoutManager(this));
        this.rvResult.setAdapter(adapter);
    }

    @Override
    public void onItemClicked(Movie movie) {
        Intent i = new Intent(this, DetailActivity.class);

        //Bundle bundle = new Bundle();

        i.putExtra("title", movie.title);
        i.putExtra("original_title", movie.originalTitle);
        i.putExtra("overview", movie.overview);
        i.putExtra("poster", movie.poster);
        i.putExtra("release", movie.releaseDate);

        this.startActivity(i);
    }
}
